import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {NavComponent} from './main-module/header/nav';
import {RouterModule} from '@angular/router';
import {appRoutes} from './routes';
import {RightHeaderComponent} from './main-module/header/right-header.component';
import {DetailsComponent} from './main-module/containers/details.component';
import {DashboardComponent} from './main-module/containers/dashboard.component';
import {LoanComponent} from './main-module/components/loan.component';
import {AttachmentsComponent} from './main-module/components/attachments.component';
import {HttpClientModule} from '@angular/common/http';
import {CustomersLoansService} from './main-module/services/customers-loans.service';
import {LoansService} from "./main-module/services/loans.service";
import {CustomersService} from "./main-module/services/customers.service";
import {FormsModule} from "@angular/forms";
import {LoanFilterPipe} from "./main-module/pipes/loan-filter.pipe";
import {CustomerComponent} from "./main-module/components/customer.component";
import {NgModule} from "@angular/core";
import {AngularFireModule} from 'angularfire2';
import {environment} from '../../src/environments/environment';
import {AngularFirestoreModule} from "angularfire2/firestore";
import {UploadService} from "./main-module/services/upload.service";
import {AttachmentService} from "./main-module/services/attachment.service";
import {AngularFireDatabaseModule} from "angularfire2/database";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ToastrModule} from 'ngx-toastr';
import {StepThreeComponent} from "./main-module/containers/step-three.component";
import {StepOneComponent} from "./main-module/containers/step-one.component";
import {StepTwoComponent} from "./main-module/containers/step-two.component";
import * as firebase from 'firebase';
import {StepService} from "./main-module/services/step.service";
import {MyDatePickerModule} from "mydatepicker";
import {TextMaskModule} from "angular2-text-mask";
import {ModalModule} from "ngx-bootstrap";
import {MyToastrService} from "./main-module/services/my-toastr.service";

firebase.initializeApp(environment.firebase);

@NgModule({
  declarations: [
    AppComponent, NavComponent, DashboardComponent, RightHeaderComponent, DetailsComponent,
    LoanComponent, AttachmentsComponent, LoanFilterPipe, CustomerComponent, StepThreeComponent,
    StepOneComponent, StepTwoComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    MyDatePickerModule,
    TextMaskModule,
    ModalModule.forRoot()
  ],
  providers: [CustomersLoansService, LoansService, CustomersService, AttachmentService, UploadService, StepService, MyToastrService],
  bootstrap: [AppComponent],
  exports: [LoanFilterPipe]
})
export class AppModule {
}
