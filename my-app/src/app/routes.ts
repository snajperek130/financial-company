import {Routes} from '@angular/router';
import {DashboardComponent} from './main-module/containers/dashboard.component';
import {DetailsComponent} from './main-module/containers/details.component';
import {StepThreeComponent} from "./main-module/containers/step-three.component";
import {StepTwoComponent} from "./main-module/containers/step-two.component";
import {StepOneComponent} from "./main-module/containers/step-one.component";

export const appRoutes: Routes = [
  {path: '', component: DashboardComponent, pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'details/:id', component: DetailsComponent},
  {path: 'step-one', component: StepOneComponent,},
  {path: 'step-two', component: StepTwoComponent},
  {path: 'step-three', component: StepThreeComponent},
];

