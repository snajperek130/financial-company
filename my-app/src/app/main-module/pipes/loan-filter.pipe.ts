import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: 'loanTypeFilter',
  pure: false
})
export class LoanFilterPipe implements PipeTransform {
  transform(loans: any, selectedType: any) {
    return loans.toJS().filter(loan => {
        return (loan.loanType === selectedType) || (selectedType === "");
      }
    );
  }
}
