export interface CustomerInterface {
  customerId: number;
  name: string;
  surname: string;
  fiscalCode: string;
  dateOfBirth: Date;
  address: string;
  phone: string;
}

export interface AttachmentInterface {
  attachmentId: number;
  attachmentName: string;
  attachmentUrl: string;
  attachmentLoanId: number;
  LoanLoanId: number;
}

export interface AttachmentToRemove {
  attachmentId: number;
  name: string;
}


export interface LoanInterface {
  loanId: number;
  customerCustomerId: number;
  totalAmount: number;
  rates: number;
  loanType: string;
}

export interface CustomerAndLoanInterface extends LoanInterface {
  customer: CustomerInterface;
}

export interface ComplexInterface extends LoanInterface {
  customer: CustomerInterface;
  attachment: AttachmentInterface [];
}
