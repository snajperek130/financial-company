import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {List} from 'immutable';
import {CustomerAndLoan} from "../models/CustomerAndLoanClass";
import {CustomerAndLoanInterface} from "../interfaces/interfaces";
import {Loan} from "../models/LoanClass";
import {Customer} from "../models/CustomerClass";

@Injectable()
export class CustomersLoansService {
  base = 'http://localhost:3000/api';
  private _customersAndLoans: BehaviorSubject<List<CustomerAndLoan>> = new BehaviorSubject(List([]));

  constructor(private http: HttpClient) {
    this.getCustomersAndLoans();
  }

  get customersAndLoans() {
    return this._customersAndLoans.asObservable();
  }

  getCustomersAndLoans() {
    this.http.get(`${this.base}/customers-and-loans`).subscribe((res: CustomerAndLoanInterface []) => {
        let customersAndLoans = res.map((data: CustomerAndLoanInterface) =>
          new CustomerAndLoan(
            {
              customer: {
                customerId: data.customer.customerId,
                name: data.customer.name,
                surname: data.customer.surname,
                fiscalCode: data.customer.fiscalCode,
                dateOfBirth: data.customer.dateOfBirth,
                address: data.customer.address,
                phone: data.customer.phone
              },
              loanId: data.loanId,
              customerCustomerId: data.customerCustomerId,
              totalAmount: data.totalAmount,
              rates: data.rates,
              loanType: data.loanType
            }
          ));
        this._customersAndLoans.next(List(customersAndLoans));
      },
      err => console.log("Error retrieving CustomersAndLoans")
    );
  }

  createCustomersAndLoans(loan: Loan, customer: Customer) {
    let nowy = new CustomerAndLoan(
      {
        customer: {
          customerId: customer.customerId,
          name: customer.name,
          surname: customer.surname,
          fiscalCode: customer.fiscalCode,
          dateOfBirth: customer.dateOfBirth,
          address: customer.address,
          phone: customer.phone
        },
        loanId: loan.loanId,
        customerCustomerId: loan.customerCustomerId,
        totalAmount: loan.totalAmount,
        rates: loan.rates,
        loanType: loan.loanType
      }
    );
    this._customersAndLoans.next(this._customersAndLoans.getValue().push(nowy));
  }

  editCustomersAndLoans(loan: Loan, customer: Customer) {
    let items = this._customersAndLoans.getValue();
    let index = items.findIndex((item: CustomerAndLoanInterface) => item.loanId === loan.loanId);
    this._customersAndLoans.next(items.set(index, new CustomerAndLoan(
      {
        customer: {
          customerId: customer.customerId,
          name: customer.name,
          surname: customer.surname,
          fiscalCode: customer.fiscalCode,
          dateOfBirth: customer.dateOfBirth,
          address: customer.address,
          phone: customer.phone
        },
        loanId: loan.loanId,
        customerCustomerId: loan.customerCustomerId,
        totalAmount: loan.totalAmount,
        rates: loan.rates,
        loanType: loan.loanType
      }
    )));
  }

}
