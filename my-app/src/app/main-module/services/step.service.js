"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
require("rxjs/add/operator/share");
var StepService = (function () {
    function StepService() {
    }
    StepService.prototype.setLoan = function (loan) {
        this.loan = loan;
    };
    StepService.prototype.setCustomer = function (customer) {
        this.customer = customer;
    };
    StepService.prototype.getLoan = function () {
        return this.loan;
    };
    StepService.prototype.getCustomer = function () {
        return this.customer;
    };
    StepService.prototype.clearService = function () {
        this.loan = null;
        this.customer = null;
    };
    StepService = __decorate([
        core_1.Injectable()
    ], StepService);
    return StepService;
}());
exports.StepService = StepService;
