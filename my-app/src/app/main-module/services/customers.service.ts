import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Customer} from "../models/CustomerClass";

@Injectable()
export class CustomersService {
  base = 'http://localhost:3000/api';

  constructor(private http: HttpClient) {
  }

  createCustomer(customer: Customer) {
    return this.http.post(`${this.base}/customer`, customer);
  }
}
