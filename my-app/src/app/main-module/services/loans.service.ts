import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {LoanInterface} from '../interfaces/interfaces';
import {Loan} from "../models/LoanClass";
import 'rxjs/add/operator/share';

@Injectable()
export class LoansService {
  base = 'http://localhost:3000/api';

  constructor(private http: HttpClient) {
  }

  createLoan(loan: LoanInterface) {
    const newLoan = new Loan(loan.loanId, loan.customerCustomerId, loan.totalAmount, loan.rates, loan.loanType);
    return this.http.post(`${this.base}/loan`, newLoan);
  }

  updateLoan(loanId: number, totalAmount: number, rates: number) {
    return this.http.post(`${this.base}/update-loan`, {loanId: loanId, totalAmount: totalAmount, rates: rates});
  }

  getDetails(loanId: number) {
    return this.http.get(`${this.base}/all/` + loanId.toString());
  }
}
