import {Injectable} from '@angular/core';
import {Loan} from "../models/LoanClass";
import 'rxjs/add/operator/share';
import {Customer} from "../models/CustomerClass";

@Injectable()
export class StepService {
  private loan: Loan;
  private customer: Customer;

  constructor() {
  }

  setLoan(loan: Loan) {
    this.loan = loan;
  }

  setCustomer(customer: Customer) {
    this.customer = customer;
  }

  getLoan() {
    return this.loan;
  }

  getCustomer() {
    return this.customer;
  }

  clearService() {
    this.loan = null;
    this.customer = null;
  }

}
