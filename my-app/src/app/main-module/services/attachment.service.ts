import {Injectable} from '@angular/core';
import 'firebase/storage';
import {HttpClient} from "@angular/common/http";
import {AttachmentInterface} from "../interfaces/interfaces";

@Injectable()
export class AttachmentService {
  base = 'http://localhost:3000/api';

  constructor(private http: HttpClient) {
  }

  createAttachment(attachment: AttachmentInterface) {
    return this.http.post(`${this.base}/create-attachment`, attachment);
  }

  deleteAttachment(id: number) {
    console.log(id)
    return this.http.post(`${this.base}/delete-attachment`, {id: id});
  }
}
