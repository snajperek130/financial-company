import {Injectable} from '@angular/core';
import 'rxjs/add/operator/share';
import {Subject} from "rxjs/Subject";

@Injectable()
export class MyToastrService {
  toastrSubject: Subject<boolean> = new Subject();

  sendNotification() {
    this.toastrSubject.next(true);
  }

}
