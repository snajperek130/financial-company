import {Injectable} from '@angular/core';
import * as  firebase from 'firebase';
import {Upload} from "../models/Upload";

@Injectable()
export class UploadService {
  private basePath = '/files';

  constructor() {
  }

  uploadFile(upload: Upload, id: number): Promise<string> {
    const storageRef = firebase.storage().ref();
    const uploadTask = storageRef.child(`${this.basePath}/${id}/${upload.file.name}`)
      .put(upload.file);

    return new Promise((resolve, reject) => {
      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot) => {
          // upload in progress
        },
        // 2.) error observer
        (error) => {
          console.log(error);
        },
        // 3.) success observer
        (): any => {
          let a: any = uploadTask;
          resolve(a.metadata_.downloadURLs);
        }
      );
    });
  }

  removeFiles(id: number, name: string): any {
    const storageRef = firebase.storage().ref();
    return storageRef.child(`${this.basePath}/${id}/${name}`).delete();
  }
}
