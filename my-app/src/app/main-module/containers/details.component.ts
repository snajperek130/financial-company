import {ChangeDetectorRef, Component, OnChanges, ViewChild,} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LoansService} from "../services/loans.service";
import {Subscription} from "rxjs/Subscription";
import {Customer} from "../models/CustomerClass";
import {Loan} from "../models/LoanClass";
import {Attachment} from "../models/AttachmentClass";
import {LoanComponent} from "../components/loan.component";
import {AttachmentsComponent} from "../components/attachments.component";
import {CustomerComponent} from "../components/customer.component";
import {ComplexInterface} from "../interfaces/interfaces";
import {ToastrService} from "ngx-toastr";
import {AttachmentService} from "../services/attachment.service";
import {CustomersLoansService} from "../services/customers-loans.service";

@Component({
  selector: 'details-component',
  templateUrl: './details.component.html'
})
export class DetailsComponent {

  id: number;
  dataSub: Subscription;
  attachments: Attachment [];
  customer: Customer;
  loan: Loan;
  validation = false;

  @ViewChild('customerDetails') customerChild: CustomerComponent;
  @ViewChild('loanDetails') loanChild: LoanComponent;
  @ViewChild('attachmentsDetails') attachmentsChild: AttachmentsComponent;

  constructor(private route: ActivatedRoute, loansService: LoansService, private toastr: ToastrService,
              private customersLoansService: CustomersLoansService, private cdr: ChangeDetectorRef) {
    this.id = route.snapshot.params['id'];
    this.dataSub = loansService.getDetails(route.snapshot.params['id'])
      .subscribe((data: ComplexInterface) => {
        this.customer = data[0].customer;
        this.attachments = data[0].attachment;
        this.loan = new Loan(data[0].loanId, data[0].customerCustomerId, data[0].totalAmount, data[0].rates, data[0].loanType);
      });
  }

  saveAll() {
    Promise.all([
      this.attachmentsChild.uploadFiles(this.id),
      this.loanChild.uploadLoan(null)
    ]).then((value) => {
      this.customersLoansService.editCustomersAndLoans(value[1], this.customer);
      this.toastr.success('Changes were succesfully saved.', 'Saved');
    });
  }

  getLoanValidation(value: boolean) {
    this.validation = value;
    this.cdr.detectChanges();
  }
}
