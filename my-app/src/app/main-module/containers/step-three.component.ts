import {Component, ViewChild} from '@angular/core';
import {CustomerComponent} from "../components/customer.component";
import {LoanComponent} from "../components/loan.component";
import {AttachmentsComponent} from "../components/attachments.component";
import {ActivatedRoute, Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {Loan} from "../models/LoanClass";
import {Customer} from "../models/CustomerClass";
import {StepService} from "../services/step.service";
import {CustomersLoansService} from "../services/customers-loans.service";
import {MyToastrService} from "../services/my-toastr.service";

@Component({
  selector: 'step-three-component',
  templateUrl: './step-three.component.html'
})
export class StepThreeComponent {
  customer: Customer;
  loan: Loan;

  @ViewChild('customerDetails') customerChild: CustomerComponent;
  @ViewChild('loanDetails') loanChild: LoanComponent;
  @ViewChild('attachmentsDetails') attachmentsChild: AttachmentsComponent;

  constructor(private router: Router, private toastr: ToastrService, private stepThree: StepService,
              private customersLoansService: CustomersLoansService, private myToastrService: MyToastrService) {
    this.customer = stepThree.getCustomer();
    this.loan = stepThree.getLoan();
    if (!this.customer || !this.loan) {
      router.navigate(['/dashboard']);
    }
  }

  saveAll() {
    return this.customerChild.uploadCustomer().then((customer: any) => {
      return this.loanChild.uploadLoan(customer.customerId).then((loan) => {
        this.customersLoansService.createCustomersAndLoans(loan,customer);
        if(this.attachmentsChild.numberOfFilesToUpload) {
          return this.attachmentsChild.uploadFiles(loan.loanId);
        } else {
          this.myToastrService.sendNotification();
          this.router.navigate(['/dashboard']);
        }
      });
    });
  }
}
