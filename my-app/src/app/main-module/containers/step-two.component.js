"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var CustomerClass_1 = require("../models/CustomerClass");
var StepTwoComponent = (function () {
    function StepTwoComponent(stepTwo, router) {
        this.stepTwo = stepTwo;
        this.router = router;
        this.myDatePickerOptions = {
            dateFormat: 'yyyy-mm-dd',
        };
        this.customer = new CustomerClass_1.Customer();
    }
    StepTwoComponent.prototype.next = function () {
        //validation
        console.log(this.customer);
        // this.stepTwo.setCustomer(this.customer);
        // this.router.navigate(['/step-three']);
    };
    StepTwoComponent = __decorate([
        core_1.Component({
            selector: 'step-two-component',
            templateUrl: './step-two.component.html'
        })
    ], StepTwoComponent);
    return StepTwoComponent;
}());
exports.StepTwoComponent = StepTwoComponent;
