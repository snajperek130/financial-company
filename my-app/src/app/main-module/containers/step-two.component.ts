import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {StepService} from "../services/step.service";
import {Customer} from "../models/CustomerClass";
import {IMyDpOptions} from "mydatepicker";

@Component({
  selector: 'step-two-component',
  templateUrl: './step-two.component.html'
})
export class StepTwoComponent {

  constructor(private stepTwo: StepService, private router: Router) {
    this.customer = new Customer();
    if (!this.stepTwo.getLoan()) {
      this.router.navigate(['/dashboard']);
    }
  }

  myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
  };

  telephoneMask = [/[1-9]/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/];

  customer: Customer;

  next() {
    const date: any = this.customer.dateOfBirth;
    this.customer.dateOfBirth = new Date(date.formatted);
    this.stepTwo.setCustomer(this.customer);
    this.router.navigate(['/step-three']);
  }
}
