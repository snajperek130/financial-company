import {Component, OnInit} from '@angular/core';
import {CustomersLoansService} from '../services/customers-loans.service';
import {ToastrService} from "ngx-toastr";
import {MyToastrService} from "../services/my-toastr.service";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'dashboard-component',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent {
  selectedType = '';
  sendSelectedType = '';

  toastrSub: Subscription;

  constructor(private customersLoansService: CustomersLoansService, private myToastrService: MyToastrService, private toastr: ToastrService ) {
    this.toastrSub = this.myToastrService.toastrSubject.subscribe((value) => {
      if(value) {
        this.toastr.success('Changes were succesfully saved.', 'Saved');
      }
    });
  }

  filterResults() {
    this.sendSelectedType = this.selectedType;
  }
}
