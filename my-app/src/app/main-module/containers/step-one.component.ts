import {Component} from '@angular/core';
import {Loan} from "../models/LoanClass";
import {Router} from "@angular/router";
import {StepService} from "../services/step.service";

@Component({
  selector: 'step-one-component',
  templateUrl: './step-one.component.html'
})
export class StepOneComponent {

  constructor(private stepOne: StepService, private router: Router) {
    this.loan = new Loan(null, null, null, null, null);
  }

  loan: Loan;

  next() {
    this.stepOne.setLoan(this.loan);
    this.router.navigate(['/step-two']);
  }

}
