"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Loan = (function () {
    function Loan(loanId, customerCustomerId, totalAmount, rates, loanType) {
        this.loanId = loanId;
        this.customerCustomerId = customerCustomerId;
        this.totalAmount = totalAmount;
        this.rates = rates;
        this.loanType = loanType;
    }
    return Loan;
}());
exports.Loan = Loan;
