import {CustomerInterface} from "../interfaces/interfaces";
import {Record} from 'immutable';

export class Customer implements CustomerInterface {
  customerId: number;
  name: string;
  surname: string;
  fiscalCode: string;
  dateOfBirth: Date;
  address: string;
  phone: string;

  constructor(customerId?, name?, surname?, fiscalCode?, dateOfBirth?, address?, phone?) {
    this.customerId = customerId;
    this.name = name;
    this.surname = surname;
    this.fiscalCode = fiscalCode;
    this.dateOfBirth = dateOfBirth;
    this.address = address;
    this.phone = phone;
  }
}
