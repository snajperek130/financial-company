import {AttachmentInterface} from "../interfaces/interfaces";

export class Attachment implements AttachmentInterface {
  attachmentId: number;
  attachmentUrl: string;
  attachmentName: string;
  attachmentLoanId: number;
  LoanLoanId: number;
  file?: File;

  constructor(attachmentUrl: string,attachmentName: string, attachmentLoanId: number, LoanLoanId: number, file?: File ) {
    this.attachmentId = null;
    this.attachmentUrl = attachmentUrl;
    this.attachmentName = attachmentName;
    this.attachmentLoanId = attachmentLoanId;
    this.LoanLoanId = LoanLoanId;
    this.file = file;
  }

}
