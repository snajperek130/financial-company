import {CustomerInterface, CustomerAndLoanInterface} from "../interfaces/interfaces";

import {Record} from 'immutable';

const CustomerLoanRecord = Record({
  loanId: 0,
  customerCustomerId: 0,
  totalAmount: 0,
  rates: 0,
  loanType: ",",
  customer: null
});

export class CustomerAndLoan extends CustomerLoanRecord implements CustomerAndLoanInterface {
  customer: CustomerInterface;
  loanId: number;
  customerCustomerId: number;
  totalAmount: number;
  rates: number;
  loanType: string;

  constructor(props) {
    super(props);
  }
}
