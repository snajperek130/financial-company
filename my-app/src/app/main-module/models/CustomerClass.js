"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Customer = (function () {
    function Customer(customerId, name, surname, fiscalCode, dateOfBirth, address, phone) {
        this.customerId = customerId;
        this.name = name;
        this.surname = surname;
        this.fiscalCode = fiscalCode;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.phone = phone;
    }
    return Customer;
}());
exports.Customer = Customer;
