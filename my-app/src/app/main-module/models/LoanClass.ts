import {LoanInterface} from "../interfaces/interfaces";

export class Loan implements LoanInterface {
  loanId: number;
  customerCustomerId: number;
  totalAmount: number;
  rates: number;
  loanType: string;

  constructor(loanId, customerCustomerId, totalAmount, rates, loanType) {
    this.loanId = loanId;
    this.customerCustomerId = customerCustomerId;
    this.totalAmount = totalAmount;
    this.rates = rates;
    this.loanType = loanType;
  }
}
