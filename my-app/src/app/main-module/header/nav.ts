import { Component } from '@angular/core';

@Component({
  selector: 'nav-component',
  templateUrl: './nav.html'
})
export class NavComponent {
  openHideNav() {
    const mainNav = document.getElementById('row-offcanvas-right');
    mainNav.classList.toggle('active');
    mainNav.blur();
  }
}
