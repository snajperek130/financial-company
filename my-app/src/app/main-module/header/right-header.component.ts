import { Component } from '@angular/core';

@Component({
  selector: 'right-header',
  templateUrl: './right-header.component.html'
})
export class RightHeaderComponent {
  openHideNav() {
    const mainNav = document.getElementById('row-offcanvas-right');
    mainNav.classList.toggle('active');
    mainNav.blur();
  }
}
