import {Component, EventEmitter, Input, OnChanges, Output, ViewChild} from '@angular/core';
import {Loan} from "../models/LoanClass";
import {LoansService} from "../services/loans.service";
import {Form, FormGroup} from "@angular/forms";

@Component({
  selector: 'loan-details',
  templateUrl: './loan.component.html'
})
export class LoanComponent {

  @Input()
  set loan(loan: Loan) {
    this.changedLoan = loan;
    this.inputLoan = loan;
  };

  @Output() loanValidation: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('loanForm') loanForm: any;

  inputLoan: Loan;
  changedLoan: Loan;

  constructor(private loanService: LoansService) {}

  uploadLoan(customerId: number): Promise<any> {
    return new Promise((resolve) => {
      if ((this.changedLoan.loanType === this.inputLoan.loanType) && (this.changedLoan.rates === this.inputLoan.rates) && !customerId) {
        this.loanService.updateLoan(this.changedLoan.loanId, this.changedLoan.totalAmount, this.changedLoan.rates).subscribe((result: any) => {
          resolve(result);
        });
      } else {
        this.changedLoan.customerCustomerId = customerId;
        this.loanService.createLoan(this.changedLoan).subscribe((result: any) => {
          resolve(result);
        });
      }
    })
  }

  sendValidation() {
    this.loanValidation.next(!this.loanForm.nativeElement.checkValidity());
  }
}
