import {Component, Input, TemplateRef, ViewChild} from '@angular/core';
import {UploadService} from "../services/upload.service";
import {Upload} from "../models/Upload";
import {Attachment} from "../models/AttachmentClass";
import {AttachmentService} from "../services/attachment.service";
import {AttachmentToRemove} from "../interfaces/interfaces";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {Router} from "@angular/router";

@Component({
  selector: 'attachments',
  templateUrl: './attachments.component.html'
})
export class AttachmentsComponent {
  @Input() attachments: Attachment[] = [];
  attachemntsToRemove: AttachmentToRemove [] = [];
  numberOfFilesToUpload: number = 0;

  @ViewChild('modal') modal: TemplateRef<any>;

  constructor(private uploadService: UploadService, private attachmentService: AttachmentService,
              private modalService: BsModalService, private router: Router) {
  }

  modalRef: BsModalRef;
  config = {
    animated: true,
    keyboard: false,
    backdrop: true,
    ignoreBackdropClick: true
  };

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }

  closeModal() {
    this.modalService.hide(1);
  }

  removeAttachment(attachment: Attachment) {
    const index = this.attachments.findIndex((value) => {
      return attachment.attachmentName === value.attachmentName;
    });
    if (index > -1) {
      this.attachments.splice(index, 1);
      if (attachment.attachmentLoanId) {
        this.attachemntsToRemove.push({
          attachmentId: attachment.attachmentId,
          name: attachment.attachmentName
        });
      }
      else {
        this.numberOfFilesToUpload = this.numberOfFilesToUpload - 1;
      }
    }
  }

  uploadFiles(loanId: number) {
    const filesToUpload = this.attachments;
    if (this.numberOfFilesToUpload) {
      this.openModal(this.modal);
    }

    filesToUpload.forEach((attachment) => {
      if (attachment.file) {
        this.uploadService.uploadFile(new Upload(attachment.file), loanId).then((url) => {
          attachment.attachmentUrl = url[0];
          attachment.attachmentLoanId = loanId;
          attachment.LoanLoanId = loanId;
          this.attachmentService.createAttachment(attachment).subscribe(() => {
            this.numberOfFilesToUpload = this.numberOfFilesToUpload - 1;
            if (!this.numberOfFilesToUpload) {
              this.closeModal();
              this.router.navigate(['/dashboard']);
            }
          });
        });
      }
    });
    if (this.attachemntsToRemove.length) {
      this.attachemntsToRemove.forEach((attachemnt) => {
        this.uploadService.removeFiles(loanId, attachemnt.name)
          .then(() => {
            return this.attachmentService.deleteAttachment(attachemnt.attachmentId).subscribe((result) => {
              console.log(result);
            });
          });
      });
    }
    this.attachemntsToRemove = [];
  }

  fileChangeEvent(event) {
    if (event.target.files[0]) {
      const attachment = event.target.files[0];
      const fileURL = window.URL.createObjectURL(new Blob([event.target.files[0]]));
      this.attachments.push(new Attachment(fileURL, attachment.name, null, null, attachment));
      this.numberOfFilesToUpload = this.numberOfFilesToUpload + 1;

    }
  }

  downloadAttachment(attachment: Attachment) {
    let link = document.createElement('a');
    link.href = attachment.attachmentUrl;
    link.download = attachment.attachmentName;
    link.click();
  }
}
