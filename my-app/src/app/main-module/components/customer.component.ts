import {Component, Input} from '@angular/core';
import {Customer} from "../models/CustomerClass";
import {CustomersService} from "../services/customers.service";

@Component({
  selector: 'customer',
  templateUrl: './customer.component.html'
})
export class CustomerComponent {
  @Input() customer: Customer;

  constructor(private customerService: CustomersService) {}

  uploadCustomer() {
    return new Promise((resolve) => {
      this.customerService.createCustomer(this.customer).subscribe((result: any) => {;
        resolve(result);
      });
    })
  }
}
