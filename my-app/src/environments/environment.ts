// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBPmThswFJhN3ljAII86Gh4ZTOi9GK6l3w",
    authDomain: "financial-company.firebaseapp.com",
    databaseURL: "https://financial-company.firebaseio.com",
    projectId: "financial-company",
    storageBucket: "gs://financial-company.appspot.com/",
    messagingSenderId: "316979903346"
  }
};
