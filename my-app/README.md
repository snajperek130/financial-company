To start app You need to follow these steps:
1) Create in postgresSql database 'financial_company'
2) In file my-app/server/config.json update settings from Your postgres.
3) Run 'npm install' in my-app folder.
4) After installation run 'npm run start-server'.
5) Install globally 'npm install -g sequelize-cli'.
6) When server is ready run in another console 'npm run data' to migrate data to Your database.
7) Restart server and type in another console 'npm run start-front'.
8) Type http://localhost:4200 to start the app.
