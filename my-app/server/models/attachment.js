'use strict';
module.exports = (sequelize, DataTypes) => {
  const Attachment = sequelize.define('Attachment', {
    attachmentId: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    attachmentName: DataTypes.STRING,
    attachmentUrl: DataTypes.STRING
  });
  Attachment.associate = (models) => {
    Attachment.belongsTo(models.Loan, {as: 'attachment'}, {
      foreignKey: 'loanId'
    });
  };
  return Attachment;
};
