'use strict';
module.exports = (sequelize, DataTypes) => {
  const Loan = sequelize.define('Loan', {
    loanId: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    totalAmount: DataTypes.INTEGER,
    rates: DataTypes.INTEGER,
    loanType: DataTypes.STRING,
  });
  Loan.associate = (models) => {
    Loan.belongsTo(models.Customer, {as: 'customer'}, {
      foreignKey: 'customerId'
    });
    Loan.hasMany(models.Attachment, {as: 'attachment'}, {
      foreignKey: 'attachmentId'
    });
  };

  return Loan;
};
