'use strict';
module.exports = (sequelize, DataTypes) => {
  const Customer = sequelize.define('Customer', {
    customerId: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    name: DataTypes.STRING,
    surname: DataTypes.STRING,
    fiscalCode: DataTypes.STRING,
    dateOfBirth: DataTypes.DATE,
    address: DataTypes.STRING,
    phone: DataTypes.STRING
  });
  Customer.associate = (models) => {
    Customer.hasOne(models.Loan, {as: 'customer'}, {
      foreignKey: 'customerId'
    });
  };
  return Customer;
};
