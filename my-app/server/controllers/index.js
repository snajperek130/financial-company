const loan = require('./loan');
const customer = require('./customer');
const attachment = require('./attachment');

module.exports = {
  loan, customer, attachment
};
