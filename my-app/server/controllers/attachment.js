const Attachment = require('../models').Attachment;

module.exports = {
  create(req) {
    return Attachment.create({
      attachmentName: req.body.attachmentName,
      attachmentUrl: req.body.attachmentUrl,
      attachmentLoanId: req.body.attachmentLoanId,
      LoanLoanId: req.body.LoanLoanId
    });
  },
  destroy(id) {
    return Attachment.find({
      where: {
        attachmentId: id
      },
    }).then(attachment => {
      if (!attachment) {
        return res.status(404).send({
          message: 'Attachment Not Found',
        });
      }
      return attachment.destroy()
    })
  },
  list() {
    return Attachment.all()
  }
};
