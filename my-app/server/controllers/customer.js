const Customer = require('../models').Customer;

module.exports = {
  create(req) {
    return Customer
      .create({
        name: req.body.name,
        surname: req.body.surname,
        fiscalCode: req.body.fiscalCode,
        dateOfBirth: req.body.dateOfBirth,
        address: req.body.address,
        phone: req.body.phone
      });
  },
  list() {
    return Customer.all();
  }
};
