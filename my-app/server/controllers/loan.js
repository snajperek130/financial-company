const Loan = require('../models').Loan;
const Customer = require('../models').Customer;
const Attachment = require('../models').Attachment;

module.exports = {
  create(req) {
    return Loan
      .create({
        totalAmount: req.body.totalAmount,
        rates: req.body.rates,
        loanType: req.body.loanType,
        customerCustomerId: req.body.customerCustomerId
      });
  },
  getAll(loanId) {
    return Loan.findAll({
      where: [{loanId: loanId}],
      include: [
        {
          model: Customer,
          as: 'customer'
        },
        {
          model: Attachment,
          as: 'attachment'
        }
      ],
    });
  },
  listLoansAndCustomers() {
    return Loan.findAll({
      include: [
        {
          model: Customer,
          as: 'customer'
        }
      ],
    });
  },
  update(req) {
    return Loan.findOne({
      where: {
        loanId: req.body.loanId
      }
    }).then(loan => {
      return loan.update({
        totalAmount: req.body.totalAmount,
        rates: req.body.rates
      })
    })
  },
};
