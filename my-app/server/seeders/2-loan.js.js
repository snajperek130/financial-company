'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Loans', [
      {
        customerCustomerId: 1,
        totalAmount: 200,
        rates: 12,
        loanType: 'Automotive'
      },
      {
        customerCustomerId: 2,
        totalAmount: 400,
        rates: 24,
        loanType: 'Customer'
      },
      {
        customerCustomerId: 3,
        totalAmount: 1200,
        rates: 36,
        loanType: 'Mortgage'
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Loan', null, {});
  }
};
