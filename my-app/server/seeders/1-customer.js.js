'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Customers', [
      {
        name: 'Frank',
        surname: 'Herbert',
        fiscalCode: '322-123-21-12',
        dateOfBirth: new Date('01-01-1988'),
        address: 'Street 12, 11-111 City',
        phone: '321431234'
      },
      {
        name: 'Gene',
        surname: 'Wolfe',
        fiscalCode: '125-421-22-11',
        dateOfBirth: new Date('05-11-1978'),
        address: 'Street 1, 14-131 Rzeszow',
        phone: '321431234'
      },
      {
        name: 'Ursula',
        surname: 'Le Guin',
        fiscalCode: '431-112-34-11',
        dateOfBirth: new Date('03-01-2000'),
        address: 'Street 100, 1-01 Rzeszow',
        phone: '564212555'
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Customer', null, {});
  }
};
