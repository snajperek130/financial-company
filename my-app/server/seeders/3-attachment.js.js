'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Attachments', [
      {
        attachmentName: '7.jpg',
        attachmentUrl: 'https://firebasestorage.googleapis.com/v0/b/financial-company.appspot.com/o/files%2F1%2F7.jpg?alt=media&token=2d1d057b-5c9e-4d80-b73d-cbd3a60a8fce',
        attachmentLoanId: 1,
        LoanLoanId: 1
      },
      {
        attachmentName: 'attachments seed.txt',
        attachmentUrl: 'https://firebasestorage.googleapis.com/v0/b/financial-company.appspot.com/o/files%2F2%2Fattachments%20seed.txt?alt=media&token=75ff6c1f-c542-4530-ae37-37dc36600343',
        attachmentLoanId: 2,
        LoanLoanId: 2
      },
      {
        attachmentName: 'berlin.jpg',
        attachmentUrl: 'https://firebasestorage.googleapis.com/v0/b/financial-company.appspot.com/o/files%2F3%2Fberlin.jpg?alt=media&token=59df6192-72b1-4027-bed3-0bb89da74f5a',
        attachmentLoanId: 3,
        LoanLoanId: 3
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Attachment', null, {});
  }
};
