'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Attachments', {
      attachmentId: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      attachmentName: {
        type: Sequelize.STRING
      },
      attachmentUrl: {
        type: Sequelize.STRING
      },
      attachmentLoanId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Loans',
          key: 'loanId'
        }
      },
      LoanLoanId: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        type: Sequelize.DATE
      },
      updatedAt: {
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => queryInterface.dropTable('Attachments'),
};
