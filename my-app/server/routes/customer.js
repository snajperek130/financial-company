const customerController = require('../controllers').customer;

module.exports = (app) => {
  app.post('/api/customer', (req, res) => {
    customerController.create(req)
      .then(loan => res.status(201).send(loan))
      .catch(error => res.status(400).send(error));
  });
};


