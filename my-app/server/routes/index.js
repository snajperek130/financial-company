module.exports = (app) => {
  require('./loan')(app);
  require('./customer')(app);
  require('./attachment')(app);
};
