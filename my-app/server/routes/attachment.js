const attachmentController = require('../controllers').attachment;

module.exports = (app) => {
  app.post('/api/create-attachment', (req, res) => {
    attachmentController.create(req)
      .then(attachment => res.status(200).send(attachment))
      .catch(error => res.status(400).send(error));
  });
  app.post('/api/delete-attachment', (req, res) => {
    attachmentController.destroy(req.body.id)
      .then(attachment => res.status(200).send(attachment))
      .catch(error => res.status(400).send(error));
  });
};
