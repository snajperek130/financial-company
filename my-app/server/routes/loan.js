const loanController = require('../controllers').loan;
module.exports = (app) => {
  app.post('/api/loan', (req, res) => {
    loanController.create(req)
      .then(loan => res.status(201).send(loan))
      .catch(error => res.status(400).send(error));
  });
  app.post('/api/update-loan', (req, res) => {
    loanController.update(req)
      .then(loan => res.status(201).send(loan))
      .catch(error => res.status(400).send(error));
  });
  app.get('/api/all/:id', (req, res) => {
    loanController.getAll(req.params.id)
      .then(all => res.status(200).send(all))
      .catch(error => res.status(400).send(error));
  });
  app.get('/api/customers-and-loans', (req, res) => {
    loanController.listLoansAndCustomers()
      .then(all => res.status(200).send(all))
      .catch(error => res.status(400).send(error));
  });


};
